package com.example.mvvm_login;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

public class LoginObserve {
    private NavController navController;
    // Observa la barra de progreso
    public void observeProgressBar(LoginViewModel loginViewModel, LifecycleOwner lifecycleOwner, View view, ProgressBar progressBar) {
        // Observo la variable que indica si se esta haciendo login o no
        loginViewModel.mostrarProgressBar.observe( lifecycleOwner, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean haciendoLogin) {
                // Pongo la barra de progreso visible
                if (haciendoLogin) {
                    progressBar.setVisibility(view.VISIBLE);
                } else {
                    progressBar.setVisibility(view.GONE);
                }
            }
        });
    }


    // Observa el usuario
    public void observeUser(LoginViewModel loginViewModel, LifecycleOwner lifecycleOwner, Context context) {
        // Callaback usuario
        loginViewModel.usuarioIncorrecto.observe(lifecycleOwner, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean correcto) {
                if (!correcto) {
                    Toast.makeText(context, "Usuario incorrecto", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    // Observa la contraseña
    public void observerPassword(LoginViewModel loginViewModel, LifecycleOwner lifecycleOwner, Context context) {
        // Obtengo la variable de la contraseña incorrecta
        loginViewModel.passwordIncorrecto.observe(lifecycleOwner, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (!aBoolean) {
                    Toast.makeText(context, "Contraseña incorrecta", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    // Observa si el login es valido
    public void observeLoginValido(LoginViewModel loginViewModel, LifecycleOwner lifecycleOwner, NavController navController) {
        loginViewModel.loginValido.observe(lifecycleOwner, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean valido) {
                if (valido) {
                    navController.navigate(R.id.home2);

                }
            }
        });
    }
}
