package com.example.mvvm_login;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class LoginViewModel extends AndroidViewModel {
    Executor executor;
    Login login;

    MutableLiveData<Boolean> loginValido = new MutableLiveData<>();
    MutableLiveData<Boolean> usuarioIncorrecto = new MutableLiveData<>();
    MutableLiveData<Boolean> passwordIncorrecto = new MutableLiveData<>();
    MutableLiveData<Boolean> mostrarProgressBar = new MutableLiveData<>();

    public LoginViewModel(@NonNull Application application) {
        super(application);
        executor = Executors.newSingleThreadExecutor();
        login = new Login();
    }

    public void comprobar(String usuario, String password) {
        final Login.DatosUsuario datosUsuario = new Login.DatosUsuario(usuario, password);

        executor.execute(new Runnable() {
            @Override
            public void run() {
                login.comprobar(datosUsuario, new Login.Callback() {
                    @Override
                    public void cuandoEmpieceElLogin() {
                        mostrarProgressBar.postValue(true);
                    }

                    @Override
                    public void cuandoLoginValido() {
                        loginValido.postValue(true);
                    }

                    @Override
                    public void usuarioIncorrecto() {
                        usuarioIncorrecto.postValue(false);
                    }

                    @Override
                    public void passwordIncorrecto() {
                        passwordIncorrecto.postValue(false);
                    }

                    @Override
                    public void cuandoFinaliceElLogin() {
                        mostrarProgressBar.postValue(false);
                    }
                });
            }
        });
    }
}
