package com.example.mvvm_login;


public class Login {
    private static final String usuario = "juanmi";
    private static final String password = "juanmi123";

    public static class DatosUsuario {
        public String usuario;
        public String password;

        public DatosUsuario(String usuario, String password) {
            this.usuario = usuario;
            this.password = password;
        }
    }

    interface Callback {
        void cuandoLoginValido();
        void usuarioIncorrecto();
        void passwordIncorrecto();
        void cuandoEmpieceElLogin();
        void cuandoFinaliceElLogin();

    }

    // Funcion que comprueba si el usuario o contraseña son correctos
    public void comprobar(DatosUsuario datosUsuario, Callback callback){
        callback.cuandoEmpieceElLogin();
        boolean login;
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Login correcto
        if (datosUsuario.usuario.equals(this.usuario) && datosUsuario.password.equals(this.password)) {
            callback.cuandoLoginValido();
        }
        // Usuario incorrecto
        else if (!datosUsuario.usuario.equals(this.usuario)) {
            callback.usuarioIncorrecto();
        }
        // Password incorrecto
        else if (datosUsuario.usuario.equals(this.usuario) && !datosUsuario.password.equals(this.password)) {
            callback.passwordIncorrecto();
        }
        callback.cuandoFinaliceElLogin();
    }





}
