package com.example.firebase;

import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.firebase.databinding.FragmentChatBinding;
import com.example.firebase.databinding.ViewholderChatBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ChatFragment extends Fragment {
    private FragmentChatBinding binding;
    private FirebaseFirestore mDb;
    private FirebaseUser user;
    private FirebaseStorage mStorage;
    private List<Mensaje> mensajes = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return (binding = FragmentChatBinding.inflate(inflater, container, false)).getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDb = FirebaseFirestore.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();
        mStorage = FirebaseStorage.getInstance();

        ChatAdpater chatAdpater = new ChatAdpater();
        binding.recyclerView.setAdapter(chatAdpater);

        // Boton adjuntar
        binding.buttonAdjuntar.setOnClickListener(click -> {
            galeria.launch("image/*");
        });

        // Boton enviar
        binding.buttonEnviar.setOnClickListener(click -> {
            String mensaje = binding.editTextMensaje.getText().toString();
            String fecha = LocalDateTime.now().toString();

            // Añado un mensaje a la tabla
            mDb.collection("mensaje")
                    .add(new Mensaje(user.getEmail(), user.getDisplayName(), user.getPhotoUrl().toString(), mensaje, fecha));

            binding.editTextMensaje.setText("");

        });

        // Cojo los mensajes de la base de datos, y los muestro ordenados por fecha
        mDb.collection("mensaje")
                .orderBy("fecha")
                // Obtengo los datos
                .addSnapshotListener((value,error) -> {
                    mensajes.clear();
                    value.forEach(mensaje -> {
                       String email = mensaje.getString("email");
                       String nombre = mensaje.getString("autor");
                       String fecha = mensaje.getString("fecha");
                       String texto = mensaje.getString("mensaje");
                       String foto = mensaje.getString("foto");
                       String adjunto = mensaje.getString("adjunto");
                       Mensaje m = new Mensaje(email, nombre, foto, texto, fecha, adjunto);
                       mensajes.add(m);
                });
                chatAdpater.notifyDataSetChanged();
                // Muevo el recycler view hasta el ultimo mensaje
                binding.recyclerView.scrollToPosition(mensajes.size() -1);
            });

    }

    class ChatAdpater extends RecyclerView.Adapter<ChatViewHolder> {

        @NonNull
        @Override
        public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ChatViewHolder(ViewholderChatBinding.inflate(getLayoutInflater(), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ChatViewHolder holder, int position) {
            Mensaje mensaje = mensajes.get(position);

            holder.binding.autor.setText(mensaje.autor);
            holder.binding.mensaje.setText(mensaje.mensaje);
            // Tiene foto
            if (mensaje.adjunto != null) {
                holder.binding.imageViewAdjunto.setVisibility(View.VISIBLE);
                Glide.with(requireView()).load(mensaje.adjunto).into(holder.binding.imageViewAdjunto);
            }
            // Tiene mensaje
            else {
                holder.binding.imageViewAdjunto.setVisibility(View.GONE);
                holder.binding.fecha.setText(mensaje.fecha);
            }
        }

        @Override
        public int getItemCount() {
            return mensajes.size();
        }
    }

    static class ChatViewHolder extends RecyclerView.ViewHolder {
        ViewholderChatBinding binding;
        public ChatViewHolder(@NonNull ViewholderChatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private final ActivityResultLauncher<String> galeria = registerForActivityResult(new ActivityResultContracts.GetContent(), uri -> {
        mStorage.getReference("adjuntos/" + UUID.randomUUID())
                .putFile(uri)
                // Cojo la url de descarga de la imagen
                .continueWithTask(task -> task.getResult().getStorage().getDownloadUrl())
                // Cuando la tarea acaba, subo la imagen
                .addOnSuccessListener(url -> {
                    String fecha = LocalDateTime.now().toString();
                    // Añado un mensaje a la tabla
                    mDb.collection("mensaje")
                            .add(new Mensaje(user.getEmail(), user.getDisplayName(), user.getPhotoUrl().toString(), null, fecha, url.toString()));

                });
    });

}