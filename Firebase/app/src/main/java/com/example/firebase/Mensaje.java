package com.example.firebase;

public class Mensaje {
    public String email;
    public String autor;
    public String foto;
    public String mensaje;
    public String fecha;
    public String adjunto;

    public Mensaje(String email, String autor, String foto, String mensaje, String fecha) {
        this.email = email;
        this.autor = autor;
        this.foto = foto;
        this.mensaje = mensaje;
        this.fecha = fecha;
    }

    public Mensaje(String email, String autor, String foto, String mensaje, String fecha, String adjunto) {
        this.email = email;
        this.autor = autor;
        this.foto = foto;
        this.mensaje = mensaje;
        this.fecha = fecha;
        this.adjunto = adjunto;
    }
}
