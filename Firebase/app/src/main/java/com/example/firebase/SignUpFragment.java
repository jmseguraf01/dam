package com.example.firebase;

import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.firebase.databinding.FragmentSignUpBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;

import java.util.UUID;


public class SignUpFragment extends Fragment {
    private FragmentSignUpBinding binding;
    private FirebaseAuth mAuth;
    private NavController navController;
    private FirebaseStorage mStorage;
    private SignUpViewModel signUpViewModel;

    public static class SignUpViewModel extends ViewModel {
        Uri fotoUri;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return (binding = FragmentSignUpBinding.inflate(inflater, container, false)).getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        navController = Navigation.findNavController(view);
        mStorage = FirebaseStorage.getInstance();
        signUpViewModel = new ViewModelProvider(this).get(SignUpViewModel.class);
        binding.emailSignUn.setOnClickListener(click -> {
            String email = binding.email.getText().toString();
            String password = binding.password.getText().toString();
            String nombre = binding.nombre.getText().toString();

            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            mStorage.getReference("avatars/"+ UUID.randomUUID())
                                    .putFile(signUpViewModel.fotoUri)
                                    .continueWithTask(task2 -> task2.getResult().getStorage().getDownloadUrl())
                                    .addOnSuccessListener(url -> mAuth.getCurrentUser().updateProfile(new UserProfileChangeRequest.Builder().setDisplayName(nombre).setPhotoUri(url).build()));
                        } else {
                            Log.e("ABCD", task.getResult().toString());
                        }
                    });

            navController.navigate(R.id.signInFragment);
        });

        binding.foto.setOnClickListener(click -> {
            galeria.launch("image/*");
        });

        // Si la variable es diferente de null, actualizo el imageview
        if(signUpViewModel.fotoUri != null) {
            Glide.with(this).load(signUpViewModel.fotoUri).circleCrop().into(binding.foto);
        }
    }

    private final ActivityResultLauncher<String> galeria = registerForActivityResult(new ActivityResultContracts.GetContent(), uri -> {
        signUpViewModel.fotoUri = uri;
        Glide.with(this).load(uri).into(binding.foto);
    });
}