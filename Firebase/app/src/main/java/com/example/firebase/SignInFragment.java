package com.example.firebase;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.firebase.databinding.FragmentSignInBinding;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

public class SignInFragment extends Fragment {

    private FragmentSignInBinding binding;
    private NavController navController;
    private FirebaseAuth firebaseAuth;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return (binding = FragmentSignInBinding.inflate(inflater, container, false)).getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        firebaseAuth = FirebaseAuth.getInstance();

        binding.emailSignUn.setOnClickListener(click -> {
            String email = binding.email.getText().toString();
            String password = binding.password.getText().toString();
            // Registro
            firebaseAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(task -> {
                // Ha funcionado bien
                if (task.isSuccessful()) {
                    navController.navigate(R.id.chatFragment);
                } else {
                    Log.e("ABCD" , "" + task.getResult());
                    Toast.makeText(getContext(), "Error Sign in", Toast.LENGTH_LONG).show();
                }
            });
        });

        // Logear con google
        binding.googleSignIn.setOnClickListener(click -> {
            signInClient.launch(GoogleSignIn.getClient(getContext(), new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id)).build()).getSignInIntent());
        });

        // Click en registrar
        binding.registrar.setOnClickListener(clicl -> {
            navController.navigate(R.id.signUpFragment);
        });
    }

    ActivityResultLauncher<Intent> signInClient = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        try {
            FirebaseAuth.getInstance().signInWithCredential(GoogleAuthProvider.getCredential(GoogleSignIn.getSignedInAccountFromIntent(result.getData()).getResult(ApiException.class).getIdToken(), null));
        } catch (ApiException e) {}
    });

}