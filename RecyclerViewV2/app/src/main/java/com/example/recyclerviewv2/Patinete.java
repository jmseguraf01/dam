package com.example.recyclerviewv2;

public class Patinete {
    String nombre;
    String descripcion;
    float valoracion;

    public Patinete(String nombre, String descripcion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
    }
}
