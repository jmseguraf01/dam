package com.example.recyclerviewv2;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.recyclerviewv2.databinding.FragmentMostrarElementoBinding;

public class mostrarElementoFragment extends Fragment {
    FragmentMostrarElementoBinding binding;
    private PatinetesViewModel patinetesViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return (binding = FragmentMostrarElementoBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        patinetesViewModel = new ViewModelProvider(requireActivity()).get(PatinetesViewModel.class);

        patinetesViewModel.seleccionado().observe(getViewLifecycleOwner(), new Observer<Patinete>() {
            @Override
            public void onChanged(Patinete patinete) {
                binding.nombre.setText(patinete.nombre);
                binding.descripcion.setText(patinete.descripcion);
            }
        });
    }
}