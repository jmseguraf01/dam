package com.example.livedatav2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.arch.core.util.Function;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

public class SoViewModel extends AndroidViewModel {
    SistemaOperativo sistemaOperativo;

    LiveData<Integer> soImagenLiveData;

    public SoViewModel(@NonNull Application application) {
        super(application);

        sistemaOperativo = new SistemaOperativo();

        soImagenLiveData = Transformations.switchMap(sistemaOperativo.sistemaLiveData, new Function<String, LiveData<Integer>>() {
            @Override
            public LiveData<Integer> apply(String so) {
                int imagen;
                switch (so) {
                    case "WINDOWS":
                    default:
                        imagen = R.drawable.windows;
                        break;
                    case "LINUX":
                        imagen = R.drawable.linux;
                        break;
                    case "MSDOS":
                        imagen = R.drawable.msdos;
                        break;
                    case "ANDROID":
                        imagen = R.drawable.android;
                        break;
                }

                return new MutableLiveData<>(imagen);

            }
        });
    }

    LiveData<Integer> obtenerImagenSO(){
        return soImagenLiveData;
    }
}
