package com.example.livedatav2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.example.livedatav2.databinding.FragmentSistemaOperativoBinding;


public class SistemaOperativoFragment extends Fragment {

    private FragmentSistemaOperativoBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (binding = FragmentSistemaOperativoBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SoViewModel soViewModel = new ViewModelProvider(this).get(SoViewModel.class);

        soViewModel.obtenerImagenSO().observe(getViewLifecycleOwner(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer ejercicio) {
                Glide.with(SistemaOperativoFragment.this).load(ejercicio).into(binding.SistemaOperativo);
            }
        });


    }
}