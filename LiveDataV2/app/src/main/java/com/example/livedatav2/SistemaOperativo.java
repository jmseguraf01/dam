package com.example.livedatav2;

import androidx.lifecycle.LiveData;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.SECONDS;

public class SistemaOperativo {

    interface SoListener {
        void cuandoCambie(String orden);
    }

    Random random = new Random();
    ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    ScheduledFuture<?> sistemaOperativo;
    String so;

    void iniciarSistemaOperativo(SoListener soListener) {
        if (sistemaOperativo == null || sistemaOperativo.isCancelled()) {
            sistemaOperativo = scheduler.scheduleAtFixedRate(new Runnable() {
                int sistema = 0;
                @Override
                public void run() {
                    switch (sistema) {
                        case 0:
                            soListener.cuandoCambie("WINDOWS");
                            break;

                        case 1:
                            soListener.cuandoCambie("LINUX");
                            break;

                        case 2:
                            soListener.cuandoCambie("MSDOS");
                            break;

                        case 3:
                            soListener.cuandoCambie("ANDROID");
                            break;
                    }
                    sistema++;
                    if (sistema > 3) {
                        sistema = 0;
                    }
                }
            }, 0, 2, SECONDS);
        }
    }

    void pararEntrenamiento() {
        if (sistemaOperativo != null) {
            sistemaOperativo.cancel(true);
        }
    }

    LiveData<String> sistemaLiveData = new LiveData<String>() {
        @Override
        protected void onActive() {
            super.onActive();

            iniciarSistemaOperativo(new SoListener() {
                @Override
                public void cuandoCambie(String sistema) {
                    postValue(sistema);
                }
            });
        }

        @Override
        protected void onInactive() {
            super.onInactive();
            pararEntrenamiento();
        }
    };
    
}


/*
WINDOWS
LINUX
AMDROID
MSDOS

 */