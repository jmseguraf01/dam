package com.example.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class Itunes {

    class Respuesta {
        List<Document> documents;
    }

    class Document {
        String name;
        Field fields;
    }

    class Field {
        StringValue artista;
        StringValue titulo;
        StringValue portada;
    }

    class StringValue {
        String stringValue;
    }

    public static Api api = new Retrofit.Builder()
//            .baseUrl("https://itunes.apple.com/")
            .baseUrl("https://firestore.googleapis.com/v1/projects/retrofit-c9d0b/databases/(default)/documents/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(Api.class);



    public interface Api {
//        @GET("search/")
        @GET("Albums/")
//        Call<Respuesta> buscar(@Query("term") String texto);
        Call<Respuesta> buscar();
    }


}
